package com.wavelabs.service.test;

import java.sql.Time;
import java.util.Date;

import com.wavelabs.model.Bookings;
import com.wavelabs.model.SlotInfo;
import com.wavelabs.model.Status;
import com.wavelabs.model.TimeSlots;
import com.wavelabs.model.User;

public class DataBuilder {
	public static Bookings getBookings() {
		Bookings bookings = new Bookings();
		bookings.setId(1);
		bookings.setStatus(Status.available);
		bookings.setUser(getUser());
		bookings.setTimeslot(getTimeslot());
		return bookings;
	}

	@SuppressWarnings("deprecation")
	public static TimeSlots getTimeslot() {
		TimeSlots timeslot = new TimeSlots();
		timeslot.setId(1);
		timeslot.setFromTime(new Time(12, 20, 30));
		timeslot.setStatus(Status.available);
		timeslot.setUser(getUser());
		timeslot.setSlot(getSlotInfo());
		return timeslot;
	}

	public static User getUser() {
		User user = new User();
		user.setId(100);
		return user;
	}

	@SuppressWarnings("deprecation")
	public static SlotInfo getSlotInfo() {
		SlotInfo slotinfo = new SlotInfo();
		slotinfo.setId(100);
		slotinfo.setDate(new Date(2017, 07, 30));
		slotinfo.setFromTime(new Time(12, 30, 30));
		slotinfo.setUser(getUser());
		slotinfo.setToTime(new Time(14, 30, 30));
		return slotinfo;
	}
}
