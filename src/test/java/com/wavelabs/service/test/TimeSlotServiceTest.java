package com.wavelabs.service.test;


import java.sql.Time;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.service.TimeSlotService;

@RunWith(MockitoJUnitRunner.class)
public class TimeSlotServiceTest {

	@InjectMocks
	private TimeSlotService timeslotServie;

	@SuppressWarnings("deprecation")
	@Test
	public void testGetNoOfSlots() {
		Time starttime = new Time(9, 00, 00);
		Time endtime = new Time(12, 00, 00);
		int value = 6;
		int count = timeslotServie.getNoOfSlots(starttime, endtime, 30);
		Assert.assertEquals(count, value);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testGetTimings() {
		Time starttime = new Time(9, 00, 00);
		Time endtime = new Time(12, 00, 00);
		List<Time> times = timeslotServie.getTimings(starttime, endtime, 30);
		Assert.assertEquals(times.size(), 7);
	}

	/*@Test
	public void testConfirmBooking() {
		Ids id = new Ids();
		id.setReceiverId(1);
		id.setTimeslotId(1);
		Ids[] ids = { id };
		when(timeslotServie.confirmBooking(ids)).thenReturn("success");
		String msg = timeslotServie.confirmBooking(ids);
		Assert.assertEquals(msg, "success");
	}

	@Test
	public void testCancelBooking() {
		Ids id = new Ids();
		id.setReceiverId(1);
		id.setTimeslotId(1);
		Ids[] ids = { id };
		when(timeslotServie.cancelBooking(ids)).thenReturn("success");
		String msg = timeslotServie.cancelBooking(ids);
		Assert.assertEquals(msg, "success");
	}*/

	@Test
	public void testGetIds() {
		String ids = "1,2,3";
		int[] id = timeslotServie.getIds(ids);
		Assert.assertEquals(id.length, 3);
	}

	/*@Test
	public void testCreateAvailability() {
		String ids = "1,2,3";
		when(timeslotServie.createAvailability(ids)).thenReturn("done");
		String msg = timeslotServie.createAvailability(ids);
		Assert.assertEquals(msg, "done");
	}

	@Test
	public void testGetTimeSlotList() {
		String ids = "1,2,3";
		int[] id = timeslotServie.getIds(ids);
		List<TimeSlots> timeslotList = new ArrayList<TimeSlots>();
		timeslotList.add(DataBuilder.getTimeslot());
		when(timeslotServie.getTimeslots(id)).thenReturn(timeslotList);
		List<TimeSlots> list = timeslotServie.getTimeSlotList(id);
		Assert.assertEquals(list.size(), timeslotList.size());
	}*/
	
}