package com.example.demo;

import javax.servlet.Filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.DispatcherServlet;

import com.wavelabs.AuthenticationTokenCache;
import com.wavelabs.NbosFilter;
import com.wavelabs.filterCheckings.UserCheckings;
import com.wavelabs.filterPersist.ObjectPersister;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@ComponentScan(basePackages = { "com.wavelabs.resources",
		"com.wavelabs.service", "com.wavelabs.dao" })
@EnableJpaRepositories("com.wavelabs.calendar.repo")
@EntityScan("com.wavelabs.model")
@SpringBootApplication
@EnableSwagger2
public class CalendarNbosApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalendarNbosApplication.class, args);
	}
	
	@Bean
	public Filter nbosFilter() {
		return new NbosFilter();
	}
	
	@Bean
	public UserCheckings userCheckings(){
		return new UserCheckings();
	}
	
	@Bean
	public ObjectPersister userPersister(){
		return new ObjectPersister();
	}

	@Bean
	public AuthenticationTokenCache authenticationTokenCache() {
		return new AuthenticationTokenCache();
	}

	@Bean
	public ServletRegistrationBean dispatcherServletRegistration() {
		ServletRegistrationBean registration = new ServletRegistrationBean(
				dispatcherServlet(), "/*");
		registration
				.setName(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_REGISTRATION_BEAN_NAME);
		return registration;
	}

	/**
	 * To add your own servlet and map it to the root resource. Default would be
	 * root of your application(/)
	 * 
	 * @return DispatcherServlet
	 */
	@Bean
	public DispatcherServlet dispatcherServlet() {
		return new DispatcherServlet();
	}
	
	
	
}
