package com.wavelabs.filterPersist;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.calendar.repo.UserRepo;
import com.wavelabs.model.User;

/**
 * 
 * @author tharunkumarb
 */
@Component
public class ObjectPersister {

	Logger logger = Logger.getLogger(ObjectPersister.class);
	@Autowired
	UserRepo userRepo;

	public boolean persistUser(User user) {
		boolean status = false;
		try {
			userRepo.save(user);
			status = true;
		} catch (Exception e) {
			logger.info(e);
		}
		return status;
	}
}
