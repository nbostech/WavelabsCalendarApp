package com.wavelabs.resources;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.model.Bookings;
import com.wavelabs.model.SlotInfo;
import com.wavelabs.model.TokenInfo;
import com.wavelabs.model.User;
import com.wavelabs.model.UserProfile;
import com.wavelabs.service.UserService;

import io.nbos.capi.api.v0.models.RestMessage;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * This resource class is for Provider actions
 * 
 * @author tharunkumarb
 *
 */
@RestController
public class UserResource {

	@Autowired
	private UserService userService;

	public UserResource() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * This method is for creating the calendar for particular provider
	 * 
	 * @param id
	 * @param fromdate
	 * @param todate
	 * @param fromtime
	 * @param totime
	 * @param slotDuration
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/api/calendar/v1/generateCalendar/{id}")
	@ApiOperation(value = "Generates a calendar", notes = "This will creates the slot accoding to input provided by the provider")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "calendar created successfully"),
			@ApiResponse(code = 404, message = "there is some problem in slot creation"),
			@ApiResponse(code = 500, message = "Problem in server") })
	public ResponseEntity createSlots(@ApiParam(value = "Id of the provider to create slot") @PathVariable("id") int id,
			@ApiParam(value = "starting date of slot creation") @RequestParam("fromdate") String fromdate,
			@ApiParam(value = "Ending date of slot creation") @RequestParam("todate") String todate,
			@ApiParam(value = "starting time of provider availability") @RequestParam("fromtime") String fromtime,
			@ApiParam(value = "End time of provider availability") @RequestParam("totime") String totime,
			@ApiParam(value = "slot duration in minutes") @RequestParam("slotDuration") int slotDuration) {
		List<SlotInfo> createslotlist = null;
		RestMessage restMessage = new RestMessage();
		try {
			DateFormat parser = new SimpleDateFormat("dd/MM/yyyy");
			Date fromDate = parser.parse(fromdate);
			Date toDate = parser.parse(todate);
			createslotlist = userService.providerSlots(id, fromDate, toDate, Time.valueOf(fromtime),
					Time.valueOf(totime), slotDuration);
		} catch (Exception e) {
		}
		if (createslotlist == null) {
			restMessage.message = "Calendar generation failed";
			restMessage.messageCode = "500";
			return ResponseEntity.status(500).body(restMessage);
		} else {
			restMessage.message = "Calendar generated successfully";
			restMessage.messageCode = "201";
			return ResponseEntity.status(HttpStatus.CREATED).body(restMessage);

		}
	}

	/**
	 * This method will creates the availability
	 * 
	 * @param id
	 * @return
	 */

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/api/calendar/v1/createAvailability/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
	@ApiOperation(value = "create availability", notes = "This will creates the availability accoding to input provided by the provider")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "slot availability creation done"),
			@ApiResponse(code = 404, message = "there is some problem in slot creation"),
			@ApiResponse(code = 500, message = "Problem in server") })
	public ResponseEntity createavailability(
			@ApiParam(name = "id", value = "ids of the timeslots") @PathVariable("id") String id) {
		String status = null;
		RestMessage restMessage = new RestMessage();
		try {
			status = userService.createAvailability(id);
		} catch (Exception e) {

		}
		if (status == null) {
			restMessage.message = "Failed to create the availability";
			restMessage.messageCode = "500";
			return ResponseEntity.status(500).body(restMessage);
		} else {
			restMessage.message = "Successfully created the availability";
			restMessage.messageCode = "200";
			return ResponseEntity.status(200).body(restMessage);
		}
	}

	/**
	 * This method will gets the list providers
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/api/calendar/v1/provider/all", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Gets all the provider", notes = "This will gets the all the providers available in the DB")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "providers successfully got from the DB"),
			@ApiResponse(code = 404, message = "there is some problem in getting providers"),
			@ApiResponse(code = 500, message = "Problem in server") })
	public ResponseEntity getProvidersList() {
		List<User> providerList = null;
		try {
			providerList = userService.getAllProviders();
		} catch (Exception e) {

		}
		if (providerList == null) {
			RestMessage restMessage = new RestMessage();
			restMessage.message = "There is no providers";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		} else {
			return ResponseEntity.status(200).body(providerList);
		}

	}

	/**
	 * This method will gets the booked timeslot data of provider
	 * 
	 * @param id
	 * @return
	 */

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/api/calendar/v1/provider/slotdata", produces = MediaType.TEXT_PLAIN_VALUE)
	@ApiOperation(value = "slotnfo data of a provider", notes = "This will gets the particular provider's slotinfodata based on the provider_id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "The slotinfo data retrieved based on provider_id successfully"),
			@ApiResponse(code = 404, message = "there is some problem in getting slotinfo"),
			@ApiResponse(code = 500, message = "Problem in server") })
	public ResponseEntity getBookedSlotData(@RequestParam("id") int id) {
		String data = null;
		try {
			data = userService.getBookedInformation(id);
		} catch (Exception e) {

		}
		if (data == null) {
			RestMessage restMessage = new RestMessage();
			restMessage.message = "No one booked your slots";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		} else {
			return ResponseEntity.status(200).body(data);
		}
	}

	/**
	 * This method will gets the list providers
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/api/calendar/v1/updateProfile", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
	@ApiOperation(value = "update the profile of the user", notes = "This will gets the all the providers available in the DB")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "providers successfully got from the DB"),
			@ApiResponse(code = 404, message = "there is some problem in getting providers"),
			@ApiResponse(code = 500, message = "Problem in server") })
	public ResponseEntity updateProfile(@RequestAttribute("tokenInfo") TokenInfo tokenInfo,
			@RequestBody() UserProfile userProfile) {
		boolean status = false;
		try {
			status = userService.updateProfile(tokenInfo, userProfile);
		} catch (Exception e) {

		}
		if (status) {
			RestMessage restMessage = new RestMessage();
			restMessage.message = "Successfully created your profile";
			restMessage.messageCode = "200";
			return ResponseEntity.status(200).body(restMessage);
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.message = "Failed to create the profile";
			restMessage.messageCode = "500";
			return ResponseEntity.status(500).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/api/calendar/v1/user/history", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "slotnfo data of a provider", notes = "This will gets the history of a user provider_id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "The slotinfo data retrieved based on provider_id successfully"),
			@ApiResponse(code = 404, message = "there is some problem in getting history"),
			@ApiResponse(code = 500, message = "Problem in server") })
	public ResponseEntity getUserHistory(@RequestParam("id") int id) {
		List<Bookings> bookings = new ArrayList<Bookings>();
		try {
			bookings = userService.getUserHistory(id);
		} catch (Exception e) {

		}
		if (bookings == null) {
			RestMessage restMessage = new RestMessage();
			restMessage.message = "No one booked your slots";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		} else {
			return ResponseEntity.status(200).body(bookings);
		}
	}

}
