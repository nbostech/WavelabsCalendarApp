package com.wavelabs.calendar.repo;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.wavelabs.model.Bookings;
import com.wavelabs.model.Status;
import com.wavelabs.model.User;

public interface BookingsRepo extends CrudRepository<Bookings, Integer> {
	Bookings findByUserAndTimeslot(int receiverId, int timeslotId);

	@Query("select b from Bookings b inner join fetch b.timeslot t inner join fetch b.user u inner join fetch t.slot s where s.date=:date and t.fromTime=:fromtime and u.id=:uid and t.status=:status")
	Bookings findByFromTimeAndStatus(@Param("date") Date date,
			@Param("fromtime") Time fromtime, @Param("uid") int rid,
			@Param("status") Status status);

	@Query("select b from Bookings b inner join fetch b.user u inner join fetch b.timeslot t where u.id=:uid and t.id=:tid and b.status=:status")
	Bookings findByTimeSlotAndReceiver(@Param("tid") int tid,
			@Param("uid") int rid, @Param("status") Status status);

	@Query("select b from Bookings b inner join fetch b.timeslot t where t.id=:tid and b.status=:status")
	List<Bookings> findBookingsByTimeslot(@Param("tid") int id,
			@Param("status") Status status);

	@Query("select b from Bookings b inner join fetch b.user u inner join fetch b.timeslot t where u.id=:uid and t.id=:tid and b.status=:status1 and b.status=:status2")
	Bookings findByTimeSlotAndReceiverAndStatus(@Param("tid") int tid,
			@Param("uid") int rid, @Param("status1") Status status1,
			@Param("status2") Status status2);

	@Query("select b from Bookings b inner join fetch b.timeslot t where t.id=:id and b.status=:status")
	Bookings findByTimeSlot(@Param("id") int id, @Param("status") Status status);

	@Query("select b from Bookings b inner join fetch b.timeslot t inner join fetch t.slot s inner join fetch s.user u where u.id=:id and b.status=:status")
	List<Bookings> findByProvider(@Param("id") int id,
			@Param("status") Status status);
	
	@Query("select b from Bookings b where b.user_id=:userId")
	 List<Bookings> findByProvider(@Param("userId") int userId);
	
}
