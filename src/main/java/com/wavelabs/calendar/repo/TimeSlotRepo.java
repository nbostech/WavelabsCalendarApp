package com.wavelabs.calendar.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.wavelabs.model.Status;
import com.wavelabs.model.TimeSlots;

public interface TimeSlotRepo extends CrudRepository<TimeSlots, Integer> {

	List<TimeSlots> findAll();

	TimeSlots findById(int id);

	@Query("select t from TimeSlots t inner join fetch t.slot s inner join fetch s.user u where u.id=:id")
	List<TimeSlots> findTimeSlotOfProvider(@Param("id") int id);

	@Query("select t from TimeSlots t inner join fetch t.slot s inner join fetch s.user u where u.id=:id and t.status=:status")
	List<TimeSlots> findByProviderAndStatus(@Param("id") int id,
			@Param("status") Status status);

	List<TimeSlots> findByIdIn(List<Integer> ids);
}
