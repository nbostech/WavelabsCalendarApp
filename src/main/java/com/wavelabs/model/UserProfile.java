package com.wavelabs.model;

public class UserProfile {
	private UserType usertype;

	public UserType getUsertype() {
		return usertype;
	}

	public void setUsertype(UserType usertype) {
		this.usertype = usertype;
	}

}
