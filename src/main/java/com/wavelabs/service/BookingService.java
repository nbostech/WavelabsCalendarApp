package com.wavelabs.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.calendar.repo.BookingsRepo;
import com.wavelabs.model.Bookings;
import com.wavelabs.model.Status;
import com.wavelabs.model.TimeSlots;

/**
 * This class contains the all the services of the bookings
 * 
 * @author tharunkumarb
 *
 */
@Service
public class BookingService {

	@Autowired
	private UserService userService;

	@Autowired
	private BookingsRepo bookingsRepo;

	Logger logger = Logger.getLogger(BookingService.class);

	public BookingService() {

	}

	/**
	 * This method will gets the booking using receiverid and the timeslotid
	 * 
	 * @param receiverid
	 * @param timeslotid
	 * @return
	 */
	public Bookings getBooking(int receiverid, int timeslotid) {
		Bookings booking = null;
		try {
			booking = bookingsRepo.findByTimeSlotAndReceiver(timeslotid,
					receiverid, Status.process);
		} catch (Exception e) {
			logger.info("something went wrong getting bookings");
			logger.info("cause " + e.getCause());
		}
		return booking;
	}

	/**
	 * This method gets the method the by status
	 * 
	 * @param receiverid
	 * @param timeslotid
	 * @return Bookings
	 */
	public Bookings getBookingByStatus(int receiverid, int timeslotid) {
		Bookings booking = null;
		try {
			booking = bookingsRepo.findByTimeSlotAndReceiverAndStatus(
					timeslotid, receiverid, Status.process, Status.booked);
		} catch (Exception e) {
			logger.info("something went wrong getting bokings");
			logger.info("cause " + e.getCause());
		}
		return booking;
	}

	public List<String> cancelForRemainingReceivers(List<TimeSlots> timeslotlist) {
		List<String> msgData = new ArrayList<String>();
		for (TimeSlots timeslot : timeslotlist) {
			List<Bookings> bookinglist = getBookingsByTimeslot(timeslot);
			for (Bookings bookings : bookinglist) {
				bookings.setStatus(Status.cancel);
				bookingsRepo.save(bookings);
				/*msgData.add(bookings.getUser().getEmail() + ","
						+ Status.cancel.toString() + ","
						+ timeslot.getFromTime().toString());*/
			}
		}
		return msgData;
	}

	/**
	 * This method will gets the bookings by timeslots
	 * 
	 * @param timeslot
	 * @return
	 */
	public List<Bookings> getBookingsByTimeslot(TimeSlots timeslot) {
		List<Bookings> bookingslist = new ArrayList<Bookings>();
		try {
			bookingslist = bookingsRepo.findBookingsByTimeslot(
					timeslot.getId(), Status.process);
		} catch (Exception e) {
			logger.info("something went wrong getting bokings");
			logger.info("cause " + e.getCause());
		}
		return bookingslist;
	}

	/**
	 * This method gets the bookings by timeslots
	 * 
	 * @param timeSlots
	 * @return Bookings
	 */
	public Bookings getBookedBookings(TimeSlots timeSlots) {
		Bookings booking = null;
		try {
			booking = bookingsRepo.findByTimeSlot(timeSlots.getId(),
					Status.booked);
		} catch (Exception e) {
			logger.info("something went wrong getting bokings");
			logger.info("cause " + e.getCause());
		}
		return booking;
	}

	/**
	 * This method gets the list of bookings based on the provider
	 * 
	 * @param provider
	 * @return List<Bookings>
	 */

	public List<Bookings> getBookingsById(int id) {
		List<Bookings> bookings = bookingsRepo.findByProvider(id,
				Status.process);
		logger.info("getting bookings");
		logger.info(bookings);
		return bookings;
	}
}
