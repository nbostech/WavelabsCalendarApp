package com.wavelabs.service;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavelabs.calendar.repo.BookingsRepo;
import com.wavelabs.calendar.repo.TimeSlotRepo;
import com.wavelabs.model.Bookings;
import com.wavelabs.model.Ids;
import com.wavelabs.model.SlotInfo;
import com.wavelabs.model.Status;
import com.wavelabs.model.TimeSlots;
import com.wavelabs.model.User;

@Service
@Transactional
public class TimeSlotService {

	@Autowired
	UserService userService;

	@Autowired
	private TimeSlotRepo timeslotRepo;

	@Autowired
	private BookingService bookingService;

	@Autowired
	private BookingsRepo bookingsRepo;

	//@PersistenceContext
//	EntityManager entityManager;

	public TimeSlotService() {

	}

	Logger logger = Logger.getLogger(TimeSlotService.class);

	/**
	 * This method will persist the timeslots
	 * 
	 * @param slotinfo
	 * @param timings
	 */
	public void createTimeSlots(List<SlotInfo> slotList, List<Time> timings) {
		logger.info("creation of timeslots going on");
		try {

			for (int i = 0; i < slotList.size(); i++) {
				persistTimeslots(slotList.get(i), timings);
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		logger.info("Done with persisting the timeslots");
	}

	public void persistTimeslots(SlotInfo slot, List<Time> timings) {
		for (int i = 0; i < timings.size(); i++) {
			TimeSlots timeslot = new TimeSlots();
			timeslot.setFromTime(timings.get(i));
			timeslot.setStatus(Status.empty);
			timeslot.setSlot(slot);
			timeslotRepo.save(timeslot);
		}
	}

	public int getNoOfSlots(Time startTime, Time endTime, int slotDuration) {
		logger.info("finding no of timeslots based start, end time and slotduration");
		int slotinMilli = slotDuration * 60 * 1000;
		int count = 0;
		while (startTime.getTime() < endTime.getTime()) {
			startTime = new Time(startTime.getTime() + slotinMilli);
			count++;
		}
		logger.info("no of timeslots for a slot " + count);
		return count;
	}

	/**
	 * This method will creates the slot timings based on the persons per slot
	 * which is given by the provider and returns the list of timings
	 * 
	 * @param startTime
	 * @param endTime
	 * @param slotDuration
	 * @return
	 */

	public List<Time> getTimings(Time startTime, Time endTime, int slotDuration) {
		logger.info("getting timeslot timings list based on the start and end time slotduration");
		List<Time> timeslotList = new ArrayList<Time>();
		int noOfslots = getNoOfSlots(startTime, endTime, slotDuration);
		long durationtime = slotDuration * 60 * 1000;
		int count = 0;
		timeslotList.add(startTime);
		while (startTime.getTime() < endTime.getTime()) {
			if (count < noOfslots) {
				startTime = new Time(startTime.getTime() + durationtime);
				timeslotList.add(startTime);
				count++;
			}
		}
		logger.info("done with getting timings of the timeslots");
		return timeslotList;
	}

	/**
	 * This will gives the timeslot based on the timeslot id
	 * 
	 * @param id
	 * @return
	 */
	public TimeSlots getTimeSlotById(int id) {
		logger.info("getting timeslot by id going on");
		return timeslotRepo.findById(id);
	}

	/**
	 * This method will books the timeslot for the consultant with particular
	 * timeslot
	 * 
	 * @param receiverid
	 * @param timeslotid
	 * @return
	 */
	public String bookTimeSlot(int receiverid, String ids) {
		logger.info("Booking a timeslot is going on");
		return updateTimeslot(userService.getUserById(receiverid), getIds(ids));
	}

	/**
	 * This method will gets the all the timeslots in the table
	 * 
	 * @return
	 */
	public List<TimeSlots> getAllTimeSlots() {
		return timeslotRepo.findAll();
	}

	public int[] getIds(String ids) {
		String[] stringids = ids.split(",");
		int[] intids = new int[stringids.length];
		for (int i = 0; i < stringids.length; i++) {
			intids[i] = Integer.parseInt(stringids[i]);
		}
		return intids;
	}

	public List<TimeSlots> getTimeSlotList(int[] ids) {
		return getTimeslots(ids);
	}

	public List<TimeSlots> getTimeSlotsByProvider(int id) {
		return timeslotRepo.findTimeSlotOfProvider(id);
	}

	public List<TimeSlots> getBookingInfo(User provider) {
		List<TimeSlots> timeslots = timeslotRepo.findByProviderAndStatus(
				provider.getId(), Status.process);
		return timeslots;
	}

	public List<Bookings> getBookings(int id) {
		return bookingService.getBookingsById(id);
	}

	public String confirmBooking(Ids[] ids) {
		List<String> msgData = new ArrayList<String>();
		List<TimeSlots> timeslotlist = new ArrayList<TimeSlots>();
		try {
			for (int i = 0; i < ids.length; i++) {
				User receiver = userService.getUserById(ids[i].getReceiverId());
				TimeSlots timeslot = getTimeSlotById(ids[i].getTimeslotId());
				/*String status = Status.booked.toString();*/
				timeslot.setStatus(Status.booked);
				timeslot.setUser(receiver);
				timeslotRepo.save(timeslot);
				/*msgData.add(receiver.getEmail() + "," + status + ","
						+ timeslot.getFromTime().toString());*/

				Bookings booking = bookingService.getBooking(receiver.getId(),
						timeslot.getId());
				booking.setStatus(Status.booked);
				bookingsRepo.save(booking);
				timeslotlist.add(timeslot);
				logger.info("Done with confirm booking process");
			}
			msgData.addAll(bookingService
					.cancelForRemainingReceivers(timeslotlist));
			/*CommunicationThread cthread = new CommunicationThread(msgData);
			Thread thread = new Thread(cthread);
			thread.start();*/
			return " booking confirmation success";
		} catch (Exception e) {
			return "booking confirmation fail";
		}
	}

	/**
	 * This method will cancels the bookings based on the ids
	 * 
	 * @param ids
	 * @return String
	 */
	public String cancelBooking(Ids[] ids) {
		List<String> msgData = new ArrayList<String>();
		try {
			for (int i = 0; i < ids.length; i++) {
				User receiver = userService.getUserById(ids[i].getReceiverId());
				TimeSlots timeslot = getTimeSlotById(ids[i].getTimeslotId());
				/*String status = Status.cancel.toString();*/
				Bookings booking = bookingService.getBooking(receiver.getId(),
						timeslot.getId());
				booking.setStatus(Status.cancel);
				//entityManager.merge(booking);
				bookingsRepo.save(booking);
				List<Bookings> bookingslist = bookingService
						.getBookingsByTimeslot(timeslot);
				if (bookingslist.size() == 0) {
					timeslot.setStatus(Status.available);
					//entityManager.merge(timeslot);
					timeslotRepo.save(timeslot);
				}
				/*msgData.add(receiver.getEmail() +","+ status + ","
						+ timeslot.getFromTime().toString());*/
				logger.info("Done with cancel booking process");
			}
			logger.info(msgData);
		/*	CommunicationThread cthread = new CommunicationThread(msgData);
			Thread thread = new Thread(cthread);
			thread.start();*/
			return " booking cancellation success";
		} catch (Exception e) {
			e.printStackTrace();
			return "booking cancellation fail";
		}
	}

	/**
	 * This methods will gets the list of timeslots
	 * 
	 * @param ids
	 * @return
	 */
	public List<TimeSlots> getTimeslots(int[] ids) {
		List<Integer> listOfIds = new ArrayList<Integer>();
		for (int i = 0; i < ids.length; i++) {
			listOfIds.add(ids[i]);
		}
		List<TimeSlots> timeslots = timeslotRepo.findByIdIn(listOfIds);
		return timeslots;
	}

	/**
	 * this method will creates the availability means it makes the timeslots as
	 * available
	 * 
	 * @param intids
	 * @return
	 */
	public String createAvailability(String ids) {
		int[] intids = getIds(ids);
		List<TimeSlots> timeslots = getTimeslots(intids);
		try {
			for (TimeSlots timeSlot : timeslots) {
				timeSlot.setStatus(Status.available);
				//entityManager.merge(timeSlot);
				timeslotRepo.save(timeSlot);
			}
			return "success";
		} catch (Exception e) {
			return "fail";
		}

	}

	/**
	 * This method will book the timeslot for particular consultant and also
	 * updates the no of booked column in slotinfo table
	 * 
	 * @param consultantid
	 * @param timeslotid
	 * @return
	 * @throws URISyntaxException
	 */
	public String updateTimeslot(User receiver, int[] ids) {
		/*List<String> msgData = new ArrayList<String>();*/
		String status = "";
		List<TimeSlots> timeslots = getTimeSlotList(ids);
		String times = "";
		try {
			for (TimeSlots timeslot : timeslots) {
				try {
					if (isAvailable(timeslot)) {
						logger.info("timeslot availablle");
						if (checkBookings(timeslot.getSlot().getDate(),
								timeslot.getFromTime(), receiver)) {
							logger.info("booking allowed");
							times = times + timeslot.getFromTime() + " ";
							timeslot.setStatus(Status.process);
							Bookings bookings = new Bookings();
							bookings.setUser(receiver);
							bookings.setTimeslot(timeslot);
							bookings.setStatus(Status.process);
							bookingsRepo.save(bookings);
							//entityManager.merge(timeslot);
							timeslotRepo.save(timeslot);
							status = Status.process.toString();
							logger.info("booking process done");
							/*msgData.add(receiver.getEmail() + "," + status
									+ "," + timeslot.getFromTime().toString());*/
						} else {
							logger.info("booking not allowed");
							status = "You already booked this time and date with someother bookie"
									+ timeslot.getId() + "\t";
						}
					} else {
						status = "Empty timeslot booking is not allowed"
								+ timeslot.getId();
					}
				} catch (Exception e) {
					logger.info("Booking process fail");
				}
			}

			/*CommunicationThread cthread = new CommunicationThread(msgData);
			cthread.start();*/
			return status + "booking sucess";
		} catch (Exception e) {
			return "booking fail";
		}
	}

	/**
	 * This method gets the timeslots of a provider
	 * 
	 * @param provider
	 * @return
	 */
	/*
	 * public List<TimeSlots> getTimeslotsList(Provider provider) { return
	 * timeslotRepo.findTimeSlotOfProvider(provider.getId()); }
	 */

	/**
	 * This method will modifys the bookings in the calendar
	 * 
	 * @param ids
	 * @return
	 */
	public String modifyBookings(String ids) {
		List<TimeSlots> timeslotList = getTimeSlotList(getIds(ids));
		List<TimeSlots> processTimeslots = timeslotList.stream()
				.filter(timeslot -> timeslot.getStatus() == Status.process)
				.collect(Collectors.toList());
		List<TimeSlots> availableList = timeslotList.stream()
				.filter(timeslot -> timeslot.getStatus() == Status.available)
				.collect(Collectors.toList());
		List<TimeSlots> emptyTimeslots = timeslotList.stream()
				.filter(timeslot -> timeslot.getStatus() == Status.empty)
				.collect(Collectors.toList());
		try {
			try {
				makeEmptyOfProcessTimeslots(processTimeslots);
				logger.info("done with process modifications");
			} catch (Exception e) {
				logger.info("there is process timeslots");
				logger.info(e.getMessage());
			}
			try {
				makeAvailableOfEmptyTimeslots(emptyTimeslots);
				logger.info("done with empty slot modifications");
			} catch (Exception e) {
				logger.info("there is no empty timeslots");
				logger.info(e.getMessage());
			}
			try {
				makeEmptyOfAvailableTimeslots(availableList);
				logger.info("Done with available slot modifications");
			} catch (Exception e) {
				logger.info("there is no available timeslots");
				logger.info(e.getMessage());
			}
			return "Done with Modifications";
		} catch (Exception e) {
			return "Something happen in modifications";
		}
	}

	/**
	 * this method will make the available timeslots to empty
	 * 
	 * @param availableList
	 */
	private void makeEmptyOfAvailableTimeslots(List<TimeSlots> availableList) {
		for (TimeSlots timeSlots : availableList) {
			timeSlots.setStatus(Status.empty);
			//entityManager.merge(timeSlots);
			timeslotRepo.save(timeSlots);
		}
	}

	/**
	 * this method make the empty timeslots as available
	 * 
	 * @param emptyTimeslots
	 */

	private void makeAvailableOfEmptyTimeslots(List<TimeSlots> emptyTimeslots) {
		for (TimeSlots timeSlots : emptyTimeslots) {
			timeSlots.setStatus(Status.available);
			//entityManager.merge(timeSlots);
			timeslotRepo.save(timeSlots);
		}
	}

	/**
	 * This method makes the proess timeslots as empty
	 * 
	 * @param processTimeslots
	 */

	private void makeEmptyOfProcessTimeslots(List<TimeSlots> processTimeslots) {
		/*List<String> msgData = new ArrayList<String>();*/
		for (TimeSlots timeSlots : processTimeslots) {
			List<Bookings> bookingList = bookingService
					.getBookingsByTimeslot(timeSlots);
			for (Bookings bookings : bookingList) {
				/*User receiver = bookings.getUser();
				String status = Status.decline.toString();*/
				bookings.setStatus(Status.decline);
				/*entityManager.merge(bookings);
				entityManager.flush();*/
				bookingsRepo.save(bookings);
				/*msgData.add(receiver.getEmail() + "," + status + ","
						+ timeSlots.getFromTime().toString());*/
				logger.info("Done with cancel bookings which are in process");
			}
			timeSlots.setStatus(Status.empty);
			/*entityManager.merge(timeSlots);
			entityManager.flush();*/
			timeslotRepo.save(timeSlots);
		}
		/*CommunicationThread cthread = new CommunicationThread(msgData);
		Thread thread = new Thread(cthread);
		thread.start();*/
		//entityManager.clear();
	}

	/**
	 * this method will decline bookings process
	 * 
	 * @param ids
	 * @return
	 */
	public String declineBookings(String ids) {
		/*List<String> msgData = new ArrayList<String>();*/
		List<TimeSlots> timeslotList = getTimeSlotList(getIds(ids));
		String message = "";
		for (TimeSlots timeSlots : timeslotList) {
			Date date1 = timeSlots.getSlot().getDate();
			System.out.println(date1);
			String stringdate = LocalDate.now().toString();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date2 = null;
			try {
				date2 = sdf.parse(stringdate);
			} catch (ParseException e) {
				logger.info("wrong in date parsing");
			}
			if (date1.compareTo(date2) > 0) {
				logger.info("decline allowed");
				Bookings booking = bookingService.getBookedBookings(timeSlots);
				/*User receiver = booking.getUser();
				String status = Status.decline.toString();*/
				booking.setStatus(Status.decline);
				/*entityManager.merge(booking);
				entityManager.flush();*/
				bookingsRepo.save(booking);
				
				/*msgData.add(receiver.getEmail() + "," + status + ","
						+ timeSlots.getFromTime().toString());*/
				logger.info("Done with decline booking process");
				timeSlots.setStatus(Status.empty);
				timeSlots.setUser(null);
				/*entityManager.merge(timeSlots);
				entityManager.flush();*/
				timeslotRepo.save(timeSlots);
			} else {
				message = "Decline not allowed for this timeslot\t";
			}
		}
		/*CommunicationThread cthread = new CommunicationThread(msgData);
		Thread thread = new Thread(cthread);
		thread.start();*/
		//entityManager.clear();
		return message + "Done with decline process";
	}

	/**
	 * This metho will checks the bookings
	 * 
	 * @param date
	 * @param fromTime
	 * @param receiver
	 * @return boolean
	 */
	private boolean checkBookings(Date date, Time fromTime, User receiver) {
		Bookings bookings = null;
		try {
			bookings = bookingsRepo.findByFromTimeAndStatus(date, fromTime,
					receiver.getId(), Status.process);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		if (bookings == null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * this method checks the timeslot is available or not
	 * 
	 * @param timeslot
	 * @return boolean
	 */
	private static boolean isAvailable(TimeSlots timeslot) {
		if (timeslot.getStatus() == Status.available
				|| timeslot.getStatus() == Status.process) {
			return true;
		} else {
			return false;
		}
	}
}
