package com.wavelabs.service;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.calendar.repo.SlotInfoRepo;
import com.wavelabs.model.SlotInfo;
import com.wavelabs.model.User;

@Service
public class SlotInfoService {

	@Autowired
	private TimeSlotService timeslotService;

	@Autowired
	private SlotInfoService slotinfoService;

	@Autowired
	SlotInfoRepo slotInfoRepo;

	public SlotInfoService() {

	}

	static Logger logger = Logger.getLogger(SlotInfoService.class);

	public List<SlotInfo> createSlots(User provider, Date fromdate,
			Date todate, Time fromtime, Time totime, int slotDuration) {
		logger.info("creation of availability is going on");
		List<SlotInfo> listOfSlots = new ArrayList<SlotInfo>();
		List<Time> timingslist = timeslotService.getTimings(fromtime,
				totime, slotDuration);
		try {
			List<Date> datesList = slotinfoService.getListOfDates(fromdate,
					todate);
			for (int i = 0; i < datesList.size(); i++) {
				SlotInfo slotinfo = new SlotInfo();
				slotinfo.setFromTime(fromtime);
				slotinfo.setToTime(totime);
				slotinfo.setUser(provider);
				slotinfo.setDate(datesList.get(i));
				slotInfoRepo.save(slotinfo);
				listOfSlots.add(slotinfo);
				logger.info("object info:"+slotinfo);
			}
			timeslotService.createTimeSlots(listOfSlots, timingslist);
		} catch (Exception e) {
			logger.warn("Something went wrong in the slot creation");
			logger.info("Reason :" + e.getMessage());
		}
		return listOfSlots;
	}

	/**
	 * this method will give you the all the list of dates from between two
	 * dates in the form of list
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<Date> getListOfDates(Date startDate, Date endDate) {
		logger.info("getting list of dates from between two dates is going on");
		List<Date> dates = new ArrayList<Date>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startDate);
		while (calendar.getTime().before(endDate)) {
			Date result = calendar.getTime();
			dates.add(result);
			calendar.add(Calendar.DATE, 1);
		}
		dates.add(endDate);
		logger.info("done with getting list of dates");
		return dates;
	}

}
