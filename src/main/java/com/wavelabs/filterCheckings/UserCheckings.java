package com.wavelabs.filterCheckings;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.calendar.repo.UserRepo;
import com.wavelabs.model.User;

@Component
public class UserCheckings {

	private static final Logger logger = Logger.getLogger(UserCheckings.class);


	@Autowired
	private UserRepo userRepo;

	public boolean isUserExist(String uuid, String tenantId) {
		try {
			User user = userRepo.findByUuidAndTenantId(uuid, tenantId);
			return (user != null);
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}
}
